package com.pruebatecnica.modelos;

import javax.persistence.*;

@Entity
@Table(name="editar")
public class ProductoUpdate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "cantidad")
    private int cantidad;

    @Column(name = "dateregister")
    private String dateregister;

    @Column(name = "usuarioregister")
    private String usuarioregister;

    @Column(name = "dateedit")
    private String dateedit;

    @Column(name = "usuarioedit")
    private String usuarioedit;

    public ProductoUpdate() {
    }

    public ProductoUpdate(String nombre, int cantidad, String dateregister, String usuarioregister, String dateedit, String usuarioedit) {
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.dateregister = dateregister;
        this.usuarioregister = usuarioregister;
        this.dateedit = dateedit;
        this.usuarioedit = usuarioedit;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getDateregister() {
        return dateregister;
    }

    public void setDateregister(String dateregister) {
        this.dateregister = dateregister;
    }

    public String getUsuarioregister() {
        return usuarioregister;
    }

    public void setUsuarioregister(String usuarioregister) {
        this.usuarioregister = usuarioregister;
    }

    public String getDateedit() {
        return dateedit;
    }

    public void setDateedit(String dateedit) {
        this.dateedit = dateedit;
    }

    public String getUsuarioedit() {
        return usuarioedit;
    }

    public void setUsuarioedit(String usuarioedit) {
        this.usuarioedit = usuarioedit;
    }
}
