package com.pruebatecnica.modelos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="agregar")
public class Producto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "cantidad")
	private int cantidad;

	@Column(name = "dateregister")
	private String dateregister;

	@Column(name = "usuarioregister")
	private String usuarioregister;

	public Producto() {
	}

	public Producto(String nombre, int cantidad, String dateregister, String usuarioregister) {
		this.nombre = nombre;
		this.cantidad = cantidad;
		this.dateregister = dateregister;
		this.usuarioregister = usuarioregister;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getDateregister() {
		return dateregister;
	}

	public void setDateregister(String dateregister) {
		this.dateregister = dateregister;
	}

	public String getUsuarioregister() {
		return usuarioregister;
	}

	public void setUsuarioregister(String usuarioregister) {
		this.usuarioregister = usuarioregister;
	}
}
