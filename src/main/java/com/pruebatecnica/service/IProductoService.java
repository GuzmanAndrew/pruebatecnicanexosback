package com.pruebatecnica.service;

import java.util.List;

import com.pruebatecnica.modelos.Producto;
import com.pruebatecnica.modelos.ProductoUpdate;

public interface IProductoService {

	public List<Producto> listarProducto();
	
	public Producto guardarProducto(Producto producto);
	
	public Producto productoId(Long id);

	public void eliminarProducto(Long id);
}
