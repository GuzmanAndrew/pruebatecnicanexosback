package com.pruebatecnica.service.impl;

import java.util.List;

import com.pruebatecnica.modelos.ProductoUpdate;
import com.pruebatecnica.repository.IProductoUpdateDAO;
import com.pruebatecnica.service.IProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pruebatecnica.modelos.Producto;
import com.pruebatecnica.repository.IProductoDAO;

@Service
public class ProductoServiceImpl implements IProductoService {
	
	@Autowired
	IProductoDAO productoDAO;

	@Override
	public List<Producto> listarProducto() {
		return productoDAO.findAll();
	}

	@Override
	public Producto guardarProducto(Producto producto) {
		return productoDAO.save(producto);
	}

	@Override
	public Producto productoId(Long id) {
		return productoDAO.findById(id).get();
	}

	@Override
	public void eliminarProducto(Long id) {
		productoDAO.deleteById(id);
	}


}
