package com.pruebatecnica.service.impl;

import com.pruebatecnica.modelos.ProductoUpdate;
import com.pruebatecnica.repository.IProductoUpdateDAO;
import com.pruebatecnica.service.IProductoUpdateService;
import org.springframework.stereotype.Service;

@Service
public class ProductoUpdateServiceImpl implements IProductoUpdateService {

    IProductoUpdateDAO productoUpdateDAO;

    @Override
    public ProductoUpdate productoId(Long id) {
        return productoUpdateDAO.findById(id).get();
    }

    @Override
    public ProductoUpdate actualizarProducto(ProductoUpdate producto) {
        return productoUpdateDAO.save(producto);
    }
}
