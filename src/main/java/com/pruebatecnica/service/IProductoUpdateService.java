package com.pruebatecnica.service;

import com.pruebatecnica.modelos.Producto;
import com.pruebatecnica.modelos.ProductoUpdate;

public interface IProductoUpdateService {
    public ProductoUpdate productoId(Long id);
    public ProductoUpdate actualizarProducto(ProductoUpdate producto);
}
