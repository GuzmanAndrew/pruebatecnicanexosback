package com.pruebatecnica.controller;

import java.util.List;

import com.pruebatecnica.modelos.ProductoUpdate;
import com.pruebatecnica.service.impl.ProductoUpdateServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.pruebatecnica.modelos.Producto;
import com.pruebatecnica.service.impl.ProductoServiceImpl;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class ProductoController {

	@Autowired
	ProductoServiceImpl productoServiceImpl;

	@Autowired
	ProductoUpdateServiceImpl productoUpdateService;
	
	@GetMapping("/listar/productos")
	public List<Producto> listarProductos(){
		return productoServiceImpl.listarProducto();
	}
	
	@PostMapping("/guardar/producto")
	public Producto guardarProducto(@RequestBody Producto producto) {
		return productoServiceImpl.guardarProducto(producto);
	}
	
	@GetMapping("/producto/{id}")
	public Producto productoId(@PathVariable(name = "id") Long id) {
		return productoServiceImpl.productoId(id);
	}

	@GetMapping("/productoUpdate/{id}")
	public ProductoUpdate productoUpdateId(@PathVariable(name = "id") Long id) {
		return productoUpdateService.productoId(id);
	}
	
	@PutMapping("/actualizar/{id}")
	public ProductoUpdate actualizarProducto(@PathVariable(name = "id") Long id, @RequestBody ProductoUpdate producto) {
		ProductoUpdate productoSelect;
		ProductoUpdate productoUpdate;

		productoSelect = productoUpdateService.productoId(id);
		productoSelect.setNombre(producto.getNombre());
		productoSelect.setCantidad(producto.getCantidad());
		productoSelect.setDateregister(producto.getDateregister());
		productoSelect.setUsuarioregister(producto.getUsuarioregister());
		productoSelect.setDateedit(producto.getDateedit());
		productoSelect.setUsuarioedit(producto.getUsuarioedit());

		productoUpdate = productoUpdateService.actualizarProducto(productoSelect);

		return productoUpdate;
	}

	@DeleteMapping("/producto/{id}")
	public void productoDelete(@PathVariable(name = "id") Long id) {
		productoServiceImpl.eliminarProducto(id);
	}
}
