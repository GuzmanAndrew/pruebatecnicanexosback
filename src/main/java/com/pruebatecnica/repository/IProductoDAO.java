package com.pruebatecnica.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pruebatecnica.modelos.Producto;

public interface IProductoDAO extends JpaRepository<Producto, Long>{

}
