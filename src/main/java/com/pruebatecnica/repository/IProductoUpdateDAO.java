package com.pruebatecnica.repository;

import com.pruebatecnica.modelos.ProductoUpdate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IProductoUpdateDAO extends JpaRepository<ProductoUpdate, Long> {
}
